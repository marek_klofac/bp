# Marek Klofáč - Validace historických modelů ve virtuální realitě

## Zadání

Věnná města českých královen je projekt zabývající se zobrazením historického prostředí ve virtuální realitě.

- Analyzujte dříve vytvořený schvalovací model v rámci projektu VMČK.
- Analyzujte systémy virtuální reality a nástroje pro vývoj ve virtuální realitně v Unity3D.
- Pomocí metod softwarových inženýrství navrhněte sady vhodných interakcí s historickými modely ve virtuální realitě se zaměřením na uživatelskou skupinu modelářů, grafiků a historiků.
- Implementujte prototyp aplikace s navrženými interakcemi v Unity3D ve virtuální realitě.
- Podrobte a vyhodnoťte prototyp vhodným uživatelským testům.

## Text bakalářské práce
Je možné ho najít [ZDE](/text/BP_Marek_Klofac_2020.pdf)

## Spuštění aplikace
Nutno spouštět na Windows 10 a mít zapojené VR brýle do počítače. Není nutné předem spouštět žádný jiný software (SteamVR se například spustí sám se spuštěním aplikace). Podporovány by měli být zařízení od firmy HTC Vive a Oculus. Ostatní netestováno.

Pro vlastní spuštění si stačí z odkazu níže stáhnout Launcher, který sám stáhne nejnovější verzi hry a nainstaluje ji.

[! STAŽENÍ LAUNCHERU !](https://www.dropbox.com/s/u3hsfw0umlhqn7g/VM%C4%8CK%20VR%20Viewer%20Launcher.zip?dl=1)

Poté je nutné se přihlasit buď pomocí soukromého Google účtu nebo pomocí školního účtu (@fit.cvut.cz). Autorizujete se tím aplikaci, aby mohla využívat privátní API projektu VMČK.