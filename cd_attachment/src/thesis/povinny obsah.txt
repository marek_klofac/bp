* Povinné části závěrečné práce
	- Zadání závěreční práce.
	- Abstrakt a klíčová slova v českém/slovenském a anglickém jazyce.
	- Studentem podepsané prohlášení o samostatném zpracování a úplnosti citací použitých pramenů – varianty textu prohlášení v záložce Ke stažení.
	- Řešení zadaného úkolu.
	- Závěrečné hodnocení výsledků.
	- Seznam literatury.
	- Obsah včetně seznamu příloh.

* Rozsah závěrečné práce
	- Doporučený rozsah pro bakalářskou práci je 30-100 obsahového textu (bez obsahu, poznámkového aparátu, příloh)
	- Doporučený rozsah pro diplomovou práci je 50-150 obsahového textu (bez obsahu, poznámkového aparátu, příloh)

* Výtisk závěrečné práce
	- Závěrečná práce musí být dodána ve dvou výtiscích v pevné vazbě. Oba výtisky musejí obsahovat zadání ZP.
	- Oba výtisky musí mít podepsané prohlášení – máte možnost vybrat si z několika verzí textu prohlášení (záložka Ke stažení), včetně prohlášení o vzdání se licence – podmínky se domlouvají a smlouva se uzavírá na katedře obhajoby.
	- Elektronická verze a zdrojové kódy musí být zapsány na přiloženém médiu:
		- Doporučená média: SD karta, CD, DVD, plochý USB disk (kvůli následné archivaci).
		- Přiložené médium musí být pevně spojeno s prací (např. vložené do pouzdra vlepeného na vnitřní straně zadní desky vazby).
		- Médium musí být označeno alespoň příjmením autora ZP a rokem nebo semestrem odevzdání.

* Formální náležitosti
	- používání symbolů, definice pojmů předcházející jejich použití, začlenění obsahu, seznamu obrázků, tabulek, použitých symbolů, případně indexu, správně vložené odkazy na použitou literaturu či jiné informační zdroje, ze kterých bylo čerpáno, bibliograficky korektní seznam použité literatury apod.
	- pro podporu sazby ZP mají studenti doporučeno využít fakultní LaTeXové a TeXové šablony
doporučené řádkování je 1,5