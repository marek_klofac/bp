\changetocdepth {3}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {english}{}
\babel@toc {czech}{}
\babel@toc {english}{}
\babel@toc {czech}{}
\contentsline {chapter}{{\' U}vod}{1}{chapter*.7}%
\contentsline {chapter}{\chapternumberline {1}Cíl práce}{3}{chapter.1}%
\contentsline {chapter}{\chapternumberline {2}Analýza}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}Backend projektu VMČK}{5}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Privátní API}{6}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Reprezentace dat}{7}{subsection.2.1.2}%
\contentsline {section}{\numberline {2.2}Schvalovací proces 3D modelů}{10}{section.2.2}%
\contentsline {paragraph}{Historik}{10}{section*.8}%
\contentsline {paragraph}{Modelář}{10}{section*.9}%
\contentsline {paragraph}{Grafik}{10}{section*.10}%
\contentsline {section}{\numberline {2.3}Interní stav 3D objektů}{17}{section.2.3}%
\contentsline {paragraph}{UNFINISHED}{17}{section*.11}%
\contentsline {paragraph}{FINISHED}{17}{section*.12}%
\contentsline {paragraph}{SUBMITTED}{17}{section*.13}%
\contentsline {paragraph}{DECLINED}{17}{section*.14}%
\contentsline {paragraph}{ACCEPTED}{17}{section*.15}%
\contentsline {paragraph}{FINALIZED}{17}{section*.16}%
\contentsline {section}{\numberline {2.4}Cílová hardwarová platforma}{18}{section.2.4}%
\contentsline {paragraph}{Oculus VR}{18}{section*.17}%
\contentsline {paragraph}{HTC \& Valve}{19}{section*.18}%
\contentsline {paragraph}{Ostatní zařízení}{20}{section*.19}%
\contentsline {section}{\numberline {2.5}Vývojové prostředí Unity}{22}{section.2.5}%
\contentsline {paragraph}{Awake()}{23}{section*.20}%
\contentsline {paragraph}{Start()}{23}{section*.21}%
\contentsline {paragraph}{Update()}{24}{section*.22}%
\contentsline {paragraph}{StartCoroutine()}{24}{section*.23}%
\contentsline {chapter}{\chapternumberline {3}Návrh}{27}{chapter.3}%
\contentsline {section}{\numberline {3.1}Autentizace a autorizace}{27}{section.3.1}%
\contentsline {paragraph}{OAuth2 přihlašovací proces}{27}{section*.24}%
\contentsline {paragraph}{Launcher}{28}{section*.25}%
\contentsline {section}{\numberline {3.2}Pohyb hráče a avatar}{31}{section.3.2}%
\contentsline {section}{\numberline {3.3}Uživatelské rozhraní}{32}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Typy UI v Unity Engine 3D}{32}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}Seznam všech struktur}{33}{subsection.3.3.2}%
\contentsline {subsection}{\numberline {3.3.3}Detail struktury}{33}{subsection.3.3.3}%
\contentsline {subsection}{\numberline {3.3.4}Detail 3D objektu}{35}{subsection.3.3.4}%
\contentsline {subsection}{\numberline {3.3.5}Detail schvalovacího procesu}{35}{subsection.3.3.5}%
\contentsline {subsection}{\numberline {3.3.6}Informace o uživateli a záhlaví}{36}{subsection.3.3.6}%
\contentsline {subsection}{\numberline {3.3.7}Popis ovládání}{36}{subsection.3.3.7}%
\contentsline {section}{\numberline {3.4}Sada nástrojů pro komentování}{37}{section.3.4}%
\contentsline {subsection}{\numberline {3.4.1}Mikrofon}{37}{subsection.3.4.1}%
\contentsline {subsection}{\numberline {3.4.2}Štětec}{37}{subsection.3.4.2}%
\contentsline {subsection}{\numberline {3.4.3}Metr}{37}{subsection.3.4.3}%
\contentsline {subsection}{\numberline {3.4.4}Fotoaparát}{38}{subsection.3.4.4}%
\contentsline {subsection}{\numberline {3.4.5}Menu pro výběr nástroje}{38}{subsection.3.4.5}%
\contentsline {section}{\numberline {3.5}Komunikace s privátním API}{38}{section.3.5}%
\contentsline {paragraph}{/auth/googlesigin}{38}{section*.26}%
\contentsline {paragraph}{/structures}{39}{section*.27}%
\contentsline {paragraph}{/models}{40}{section*.28}%
\contentsline {paragraph}{/users}{40}{section*.29}%
\contentsline {paragraph}{/approvals}{40}{section*.30}%
\contentsline {paragraph}{/comments}{42}{section*.31}%
\contentsline {section}{\numberline {3.6}Interakce s historickými předměty}{42}{section.3.6}%
\contentsline {paragraph}{Uchopení}{42}{section*.32}%
\contentsline {paragraph}{Wireframe}{42}{section*.33}%
\contentsline {paragraph}{Škálování}{43}{section*.34}%
\contentsline {chapter}{\chapternumberline {4}Realizace}{45}{chapter.4}%
\contentsline {section}{\numberline {4.1}Launcher a přihlášení}{45}{section.4.1}%
\contentsline {section}{\numberline {4.2}Virtuální prostředí}{48}{section.4.2}%
\contentsline {section}{\numberline {4.3}Pohyb ve virtuálním prostoru}{49}{section.4.3}%
\contentsline {section}{\numberline {4.4}Uživatelské rozhraní}{51}{section.4.4}%
\contentsline {section}{\numberline {4.5}Interakce s objekty}{55}{section.4.5}%
\contentsline {section}{\numberline {4.6}Nástroje}{56}{section.4.6}%
\contentsline {paragraph}{Mikrofon}{57}{section*.35}%
\contentsline {paragraph}{Štětec}{57}{section*.36}%
\contentsline {paragraph}{Metr}{58}{section*.37}%
\contentsline {chapter}{\chapternumberline {5}Testování prototypu}{61}{chapter.5}%
\contentsline {section}{\numberline {5.1}Testovací scénáře}{61}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Přihlášení}{61}{subsection.5.1.1}%
\contentsline {subsection}{\numberline {5.1.2}Pohyb a ovládání}{62}{subsection.5.1.2}%
\contentsline {subsection}{\numberline {5.1.3}Zkoumání historického modelu}{63}{subsection.5.1.3}%
\contentsline {section}{\numberline {5.2}Výsledky}{64}{section.5.2}%
\contentsline {paragraph}{Přihlášení}{64}{section*.38}%
\contentsline {paragraph}{Pohyb a ovládání}{65}{section*.39}%
\contentsline {paragraph}{Zkoumání historického modelu}{65}{section*.40}%
\contentsline {chapter}{Z{\' a}v{\v e}r}{67}{chapter*.41}%
\contentsline {chapter}{Literatura}{69}{chapter*.42}%
\contentsline {appendix}{\chapternumberline {A}Seznam použitých zkratek}{73}{appendix.A}%
\contentsline {appendix}{\chapternumberline {B}Obsah přiloženého CD}{75}{appendix.B}%
