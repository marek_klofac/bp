# Marek Klofáč - Validace historických modelů ve virtuální realitě

## Zadání

Věnná města českých královen je projekt zabývající se zobrazením historického prostředí ve virtuální realitě.

- Analyzujte dříve vytvořený schvalovací model v rámci projektu VMČK.
- Analyzujte systémy virtuální reality a nástroje pro vývoj ve virtuální realitně v Unity3D.
- Pomocí metod softwarových inženýrství navrhněte sady vhodných interakcí s historickými modely ve virtuální realitě se zaměřením na uživatelskou skupinu modelářů, grafiků a historiků.
- Implementujte prototyp aplikace s navrženými interakcemi v Unity3D ve virtuální realitě.
- Podrobte a vyhodnoťte prototyp vhodným uživatelským testům.



## Zdrojové soubory
Vzhledem k velikosti celého projektu jsem do složky src/impl vložil pouze odkaz na internetový repozitář, obsahující všechny zdrojové kódy. Tento odkaz pro jistotu uvádím i zde:

- https://gitlab.fit.cvut.cz/xchludil-bp-dp/klofama1
- https://gitlab.com/marek_klofac/bp (záložní veřejný repozitář)



## Spuštění aplikace


! DŮLEŽITÉ !
Přidejte složku s Launcherem a VR aplikací do vyjímek vašeho Antiviru. Jinak selže stahování herních souborů a interni komunikace s API!


Nutno spouštět na Windows 10 a mít zapojené VR brýle do počítače. Není nutné předem spouštět žádný jiný software (SteamVR se například spustí sám se spuštěním aplikace). Podporovány by měli být zařízení od firmy HTC Vive a Oculus. Ostatní netestováno.

Pro vlastní spuštění stačí rozbalit build.zip a spustit launcher.exe. V archivu se nachází Launcher, který sám stáhne nejnovější verzi hry a nainstaluje ji.

Poté je nutné se přihlasit buď pomocí soukromého Google účtu nebo pomocí školního účtu (@fit.cvut.cz). Autorizujete se tím aplikaci, aby mohla využívat privátní API projektu VMČK.