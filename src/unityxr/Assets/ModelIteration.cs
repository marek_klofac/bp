﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModelIteration : MonoBehaviour
{
    public Sprite approveNull;
    public Sprite approvedYes;
    public Sprite approvedNo;
    public Text modelName;
    public Image approvedByHistorian;
    public Image approvedByGraphician;
    public ModelIterationJSONDeserialized data;

    private void Start()
    {
        GetComponentInChildren<Button>().onClick.AddListener(ShowDetail);
    }

    public void Initialize(ModelIterationJSONDeserialized _data)
    {
        data = _data;
        modelName.text = data.name;

        // Solve UI images of approval
        if(data.historianApprovingId == "" && data.historianDecliningId == "")
        {
            approvedByHistorian.sprite = approveNull;
        }
        else
        {
            approvedByHistorian.sprite = data.historianApprovingId == "" ? approvedNo : approvedYes;
        }
        if (data.graphicianApprovingId == "" && data.graphicianDecliningId == "")
        {
            approvedByGraphician.sprite = approveNull;
        }
        else
        {
            approvedByGraphician.sprite = data.graphicianApprovingId == "" ? approvedNo : approvedYes;
        }
    }

    private void ShowDetail()
    {
        MainMenuManager.Instance.ShowApprovalIterationDetail(this);
    }
}
