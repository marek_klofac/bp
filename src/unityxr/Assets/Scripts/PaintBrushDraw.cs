﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class PaintBrushDraw : MonoBehaviour
{
    public XRNode changeBrushColorWithHand;
    public GameObject paintBrushHead;
    public Transform drawPositionSource;
    public float lineWidth = 0.03f;
    public Material lineMaterial;
    public float distanceThreshold = 0.05f;
    public bool IsInHand
    {
        get
        {
            return isInHand;
        }
        set
        {
            isInHand = value;
        }
    }

    private bool isInHand;
    private bool isDrawing;
    private LineRenderer currentLine;
    private List<Vector3> currentLinePositions = new List<Vector3>();
    private Vector2 touchpadInput;
    private Color currentLineColor = new Color(1f, 1f, 1f);

    // Update is called once per frame
    void Update()
    {
        if(isDrawing)
        {
            UpdateDrawing();
        }

        // Track touchpad movement for brush color change - track only if user is holding it and touching it
        if(isInHand)
        {
            InputDevice device = InputDevices.GetDeviceAtXRNode(changeBrushColorWithHand);
            device.TryGetFeatureValue(CommonUsages.primary2DAxis, out touchpadInput);

            if(touchpadInput != Vector2.zero)
            {
                ChangeBrushColor();
            }
        }
    }
    public void UpdateLine()
    {
        // Add created starting line to list of lines
        currentLinePositions.Add(drawPositionSource.position);
        currentLine.positionCount = currentLinePositions.Count;
        currentLine.SetPositions(currentLinePositions.ToArray());

        // Update the visual of line
        currentLine.startWidth = lineWidth;
        currentLine.material = lineMaterial;
        currentLine.GetComponent<LineRenderer>().startColor = currentLineColor;
        currentLine.GetComponent<LineRenderer>().endColor = currentLineColor;
    }

    public void StartDrawing()
    {
        isDrawing = true;

        // Create new line
        GameObject line = new GameObject("Line");
        currentLine = line.AddComponent<LineRenderer>();

        UpdateLine();
    }
    public void StopDrawing()
    {
        isDrawing = false;
        currentLinePositions.Clear();
        currentLine = null;
    }
    public void UpdateDrawing()
    {
        if (!currentLine || currentLinePositions.Count == 0)
        {
            return;
        }
        Vector3 lastSetPosition = currentLinePositions[currentLinePositions.Count - 1];
        if(Vector3.Distance(lastSetPosition, drawPositionSource.position) > distanceThreshold)
        {
            UpdateLine();
        }
    }

    private void ChangeBrushColor()
    {
        // Calculate and normalize angle from X & Y coordinates from the controller
        float angle = Mathf.Atan2(touchpadInput.x, touchpadInput.y) * Mathf.Rad2Deg;
        float normalAngle = angle - 90;
        if(normalAngle < 0)
        {
            normalAngle = 360 + normalAngle;
        }
        // Convert to radians and calculate max X and Y coords
        float rads = normalAngle * Mathf.PI / 180;
        float maxX = Mathf.Cos(rads);
        float maxY = Mathf.Sin(rads);

        // Calculate percentage between current X, Y coords and max
        float percentageX = Mathf.Abs(touchpadInput.x / maxX);
        float percentageY = Mathf.Abs(touchpadInput.y / maxY);

        // Prepare color in HSV space
        float hue = normalAngle / 360.0f;
        float saturation = (percentageX + percentageY) / 2;
        float value = 1f;

        // Set paintbrush tip to this new color
        Color color = Color.HSVToRGB(hue, saturation, value);
        paintBrushHead.GetComponent<Renderer>().material.color = color;
        currentLineColor = color;
    }
}
