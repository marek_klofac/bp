﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WireframeRenderer : MonoBehaviour
{
    public bool active = false;

    private List<Renderer> renderers;
    private Shader defaultShader;
    private Shader wireframeShader;

    public void Start()
    {
        // Initialize renderers list
        renderers = new List<Renderer>();

        // Get all children renderers and add them to list for better manipulation
        Component [] childRenderers = GetComponentsInChildren(typeof(Renderer));
        foreach(Renderer r in childRenderers)
        {
            renderers.Add(r);
        }
        // Add current gameObject's renderer if it has one
        Renderer currentRenderer = GetComponent<Renderer>();
        if(currentRenderer != null)
        {
            renderers.Add(currentRenderer);
        }

        // Lookup shaders
        defaultShader = Shader.Find("Universal Render Pipeline/Lit");
        wireframeShader = Shader.Find("SuperSystems/Wireframe-Transparent");
    }
    public void ChangeShader()
    {
        if(renderers.Count > 0)
        {
            if(active)
            {
                foreach (Renderer renderer in renderers)
                {
                    renderer.material.shader = defaultShader;
                    active = false;
                }
            }
            else
            {
                foreach (Renderer renderer in renderers)
                {
                    renderer.material.shader = wireframeShader;
                    active = true;
                }
            }
        }
    }
}
