﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class LocomotionController : MonoBehaviour
{
    public XRController teleportRay;
    public InputHelpers.Button teleportActivationButton;
    public float activationThreshold = 0.1f;
    public XRRayInteractor uiInteractorRay;

    public bool teleportEnabled { get; set; } = true;

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = new Vector3();
        Vector3 norm = new Vector3();
        int index = 0;
        bool validTarget = false;

        if(teleportRay)
        {
            // Activate teleport ray only if teleport is enable and also ui interactor ray is not hovering over UI
            teleportRay.gameObject.SetActive(teleportEnabled && CheckIfActivated(teleportRay) && !uiInteractorRay.TryGetHitInfo(out pos, out norm, out index, out validTarget));
        }
    }

    public bool CheckIfActivated(XRController _controller)
    {
        InputHelpers.IsPressed(_controller.inputDevice, teleportActivationButton, out bool isActivated, activationThreshold);
        return isActivated;
    }
}
