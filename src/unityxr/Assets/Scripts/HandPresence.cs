﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR;

public class HandPresence : MonoBehaviour
{
    public UnityEvent OnPrimaryButtonPress;
    public bool showController = true;
    public InputDeviceCharacteristics controllerCharacteristics;
    public List<GameObject> controllerPrefabs;
    private InputDevice targetDevice;
    private GameObject spawnedController;
    private bool primaryButtonPressed = false;

    // Start is called before the first frame update
    void Start()
    {
        TryInitialize();
    }

    // Update is called once per frame
    void Update()
    {
        if(!targetDevice.isValid)
        {
            TryInitialize();
        }
        else
        {
            // Display controllers
            if(showController)
            {
                spawnedController.SetActive(true);
            }
            else
            {
                spawnedController.SetActive(false);
            }

            // Handle secondary button press
            if(targetDevice.TryGetFeatureValue(CommonUsages.primaryButton, out bool primaryButtonValue) && primaryButtonValue)
            {
                if(!primaryButtonPressed)
                {
                    primaryButtonPressed = true;
                    OnPrimaryButtonPress.Invoke();
                }
            }
            // Check for button release
            else
            {
                primaryButtonPressed = false;
            }
        }

        /*if(targetDevice.TryGetFeatureValue(CommonUsages.primaryButton, out bool primaryButtonValue) && primaryButtonValue)
        {
            Debug.Log("Pressing primary button.");
        }
        if(targetDevice.TryGetFeatureValue(CommonUsages.trigger, out float triggerValue) && triggerValue > 0.1f)
        {
            Debug.Log("Pressing trigger button: " + triggerValue);
        }
        if(targetDevice.TryGetFeatureValue(CommonUsages.primary2DAxis, out Vector2 primary2DAxisValue) && primary2DAxisValue != Vector2.zero)
        {
            Debug.Log("Pressing touchap: " + primary2DAxisValue);
        }*/
    }

    private void TryInitialize()
    {
        List<InputDevice> devices = new List<InputDevice>();
        InputDevices.GetDevicesWithCharacteristics(controllerCharacteristics, devices);


        foreach(var device in devices)
        {
            Debug.Log(device.name + device.characteristics);
        }

        if(devices.Count > 0)
        {
            targetDevice = devices[0];
            GameObject prefab = controllerPrefabs.Find(controller => controller.name == targetDevice.name);
            if(prefab)
            {
                spawnedController = Instantiate(prefab, transform);
            }
            else
            {
                Debug.LogError("Did not find given controller model. Using default");
                spawnedController = Instantiate(controllerPrefabs[0], transform);
            }
        }
    }
}
