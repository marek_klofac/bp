﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class XRExhibitGrabInteractable : XRGrabInteractable
{
    public XRSimpleInteractable secondHandGrab;
    public float interactorPositionTolerance = 0.001f;

    // GameObject has to be bigger than initial size / 2 and smaller than 4x initial size
    public float minimumScaleMultiplier = 2;
    public float maximumScaleMultiplier = 2;

    private Vector3 interactorPosition = Vector3.zero;
    private Quaternion interactorRotation = Quaternion.identity;
    private bool twoHandsGrabbing = false;
    private XRBaseInteractor firstHandInteractor;
    private XRBaseInteractor secondHandInteractor;
    private float lastDistance = 0;
    private float initialScale;
    private float scalingFactor;

    private void Start()
    {
        secondHandGrab.onSelectEntered.AddListener(OnSecondHandGrab);
        secondHandGrab.onSelectExited.AddListener(OnSecondHandRelease);

        // Calculate initial scaling factor dependent on initial scale
        // 0.01 is fine scaling factor for scale x = 1, take that into account
        initialScale = transform.localScale.x;
        scalingFactor = initialScale / 50;
    }
    private void Update()
    {
        // If there are currently two hands grabbing the object, scale it up or down dependt on interactors distance
        if(twoHandsGrabbing)
        {
            float currentDistance = Vector3.Distance(firstHandInteractor.transform.position, secondHandInteractor.transform.position);

            // First frame that two hands are interacting with object, set lastDistance to current
            if(Mathf.Abs(currentDistance - lastDistance) < interactorPositionTolerance)
            {
                
            }
            else
            {
                if(currentDistance < lastDistance)
                {
                    ShrinkObject();
                }
                else
                {
                    EnlargeObject();
                }
            }
            lastDistance = currentDistance;
        }
    }

    private void EnlargeObject()
    {
        // Scale up by given factor only if the current scale is less than initial * max multiplier -> cannot indefinitely enlarge object
        if(transform.localScale.x < initialScale * maximumScaleMultiplier)
        {
            transform.localScale = new Vector3(transform.localScale.x + scalingFactor, transform.localScale.y + scalingFactor, transform.localScale.z + scalingFactor);
        }
    }

    private void ShrinkObject()
    {
        // Scale down by given factor only if the current scale is more than initial * min multiplier -> cannot indefinitely shrink object
        if(transform.localScale.x > initialScale / minimumScaleMultiplier)
        {
            transform.localScale = new Vector3(transform.localScale.x - scalingFactor, transform.localScale.y - scalingFactor, transform.localScale.z - scalingFactor);

        }
    }

    public override bool IsSelectableBy(XRBaseInteractor interactor)
    {
        bool isAlreadyGrabbed = selectingInteractor && !interactor.Equals(selectingInteractor);
        return base.IsSelectableBy(interactor) && !isAlreadyGrabbed;
    }

    // Offset grab functionality - save controller position and snap it to attach transform if there is one
    protected override void OnSelectEntered(XRBaseInteractor interactor)
    {
        base.OnSelectEntered(interactor);

        // Store interactor values
        firstHandInteractor = interactor;
        interactorPosition = interactor.attachTransform.localPosition;
        interactorRotation = interactor.attachTransform.localRotation;

        // Snap it to attach transform is there is one defined, otherwise do nothing
        bool hasAttach = attachTransform != null;
        interactor.attachTransform.position = hasAttach ? attachTransform.position : transform.position;
        interactor.attachTransform.rotation = hasAttach ? attachTransform.rotation : transform.rotation;
    }

    // Offset grab functionality - object was released, restore previously saved transform of controller
    protected override void OnSelectExited(XRBaseInteractor interactor)
    {
        base.OnSelectExited(interactor);

        // Reset attachments points
        interactor.attachTransform.localPosition = interactorPosition;
        interactor.attachTransform.localRotation = interactorRotation;

        // Clear interactor
        interactorPosition = Vector3.zero;
        interactorRotation = Quaternion.identity;
    }

    // Grab with other hand in child's simple interactor script has been triggered
    public void OnSecondHandGrab(XRBaseInteractor interactor)
    {
        secondHandInteractor = interactor;
        twoHandsGrabbing = true;
    }

    // Grab with other hand in child's simple interactor script has been released
    public void OnSecondHandRelease(XRBaseInteractor interactor)
    {
        twoHandsGrabbing = false;
    }
}
