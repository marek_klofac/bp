﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class Info : MonoBehaviour
{
    public Text text;

    void Start()
    {
        string [] arguments = Environment.GetCommandLineArgs();
        foreach(string arg in arguments)
        {
            text.text += arg.ToString() + "\n";
        }
    }
}