﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.XR.Interaction.Toolkit;

public class ModelLoader : MonoBehaviour
{
    public Transform spawnPoint;
    public GameObject currentModel;
    private string currentModelID;

    public static ModelLoader Instance { get { return _instance; } }
    private static ModelLoader _instance;

    // This class is a Singleton
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void DownloadModel(string _modelID)
    {
        VMCKConnector.Instance.GetModelCallDone.AddListener(ImportModel);
        currentModelID = _modelID;
        StartCoroutine(VMCKConnector.Instance.GetModel(_modelID));
    }

    // Import object from downloaded .obj file
    public void ImportModel(string _response)
    {
        // Save obtained data via API to a temporary .obj file
        string filePath = string.Format("{0}/{1}.obj", Application.dataPath, currentModelID);
        System.IO.File.WriteAllText(filePath, _response);

        // Create empty GameObject with mesh renderer and mesh filter
        currentModel = new GameObject(currentModelID);
        currentModel.transform.position = spawnPoint.transform.position + spawnPoint.transform.forward;
        MeshRenderer renderer = currentModel.AddComponent<MeshRenderer>();
        MeshFilter filter = currentModel.AddComponent<MeshFilter>();

        // Import downloaded file via obj importer
        ObjImporter importer = new ObjImporter();
        filter.mesh = importer.ImportFile(filePath);

        // Rescale model to be normal size compared to user
        Bounds bounds = renderer.bounds;
        float largestBound;

        // Find largest bound
        if (bounds.size.x >= bounds.size.y && bounds.size.y >= bounds.size.z)
        {
            largestBound = bounds.size.x;
        }
        else if (bounds.size.y >= bounds.size.z && bounds.size.z >= bounds.size.x)
        {
            largestBound = bounds.size.y;
        }
        else
        {
            largestBound = bounds.size.z;
        }
        // Normalize other bounds by these to get model to fit inside a unit cube
        currentModel.transform.localScale = new Vector3(1 / largestBound, 1 / largestBound, 1 / largestBound);

        // Attach all important scriupts to imported GameObject, so that user can iteract with it
        BoxCollider collider = currentModel.AddComponent<BoxCollider>() as BoxCollider;
        Rigidbody rb = currentModel.AddComponent<Rigidbody>() as Rigidbody;
        XRExhibitGrabInteractable interactable = currentModel.AddComponent<XRExhibitGrabInteractable>() as XRExhibitGrabInteractable;
        WireframeRenderer wireframeRenderer = currentModel.AddComponent<WireframeRenderer>() as WireframeRenderer;

        // Create second hand grab
        GameObject secondHandGrab = new GameObject("SecondHandGrab");
        secondHandGrab.transform.SetParent(currentModel.transform, false);
        BoxCollider secondCollider = secondHandGrab.AddComponent<BoxCollider>() as BoxCollider;
        Rigidbody secondRb = secondHandGrab.AddComponent<Rigidbody>() as Rigidbody;
        XRSimpleInteractable secondInteractable = secondHandGrab.AddComponent<XRSimpleInteractable>() as XRSimpleInteractable;

        // Tweek script options for second hand grab model, make the second collider same as previous but 10% smaller, so that they dont overlap
        secondHandGrab.layer = LayerMask.NameToLayer("Second Hand Grabbable");
        secondRb.isKinematic = true;
        secondCollider.center = collider.center;
        secondCollider.size = new Vector3(collider.size.x * 0.9f, collider.size.y * 0.9f, collider.size.z * 0.9f);
        secondInteractable.colliders.Add(secondCollider);

        // Tweek script options for current model
        currentModel.layer = LayerMask.NameToLayer("Grabbable");
        rb.isKinematic = true;
        interactable.colliders.Add(collider);
        interactable.onActivate.AddListener(clicked => wireframeRenderer.ChangeShader());
        interactable.secondHandGrab = secondInteractable;


        // Download textures from VMCK backend via private API
        // TODO ...

        // Create material
        renderer.material = new Material(Shader.Find("Universal Render Pipeline/Lit"));

        VMCKConnector.Instance.GetModelCallDone.RemoveListener(ImportModel);
    }
}
