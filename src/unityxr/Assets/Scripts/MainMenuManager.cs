﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    // Header and main UI panels
    public Text windowHeader;
    public GameObject allStructuresPanel;
    public GameObject structureDetailPanel;
    public GameObject structureDetailTDObjectsPanel;
    public GameObject structureDetailApprovalPanel;
    public GameObject iterationDetailPanel;
    public GameObject newCommentPanel;
    public GameObject errorDetailPanel;
    public GameObject infoPanel;

    // UI prefabs to be spawned
    public GameObject structurePreviewPrefab;
    public GameObject tdobjectPreviewPrefab;
    public GameObject modelIterationPrefab;
    public GameObject commentPrefab;

    // UI Elements where to spawn these prefabs
    public GameObject allStructuresContent;
    public GameObject structureDetailTDObjectsContent;
    public GameObject structureDetailApprovalsContent;
    public GameObject iterationDetailContent;

    // UI elements for User info panel
    public Text userInfoName;
    public Text userInfoUsername;
    public Text userInfoEmail;
    public Text userInfoRole;

    // UI elements for Structure detail
    public Text structureDetailName;
    public Text structureDetailAuthor;
    public Text structureDetailDateCreated;
    public Text structureDetailLocation;
    public Text structureDetailDescription;

    // UI Elements for Iteration detail
    public GameObject historianActions;
    public GameObject graphicianActions;
    public Button addCommentButton;
    public Button approveAsHistorian;
    public Button declineAsHistorian;
    public Button approveAsGraphician;
    public Button declineAsGraphician;

    // UI Elements for New Comment detail
    public Button submitNewComment;
    public Text newCommentContent;

    // UI Elements for Error detail
    public Text errorDetailText;

    public static MainMenuManager Instance { get { return _instance; } }
    private static MainMenuManager _instance;

    // This class is a Singleton
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    // Async download all structures and show them in UI
    void Start()
    {
        // Hide all other UI panels, except all structures panel (starting page)
        structureDetailPanel.SetActive(false);
        iterationDetailPanel.SetActive(false);
        newCommentPanel.SetActive(false);
        errorDetailPanel.SetActive(false);
        infoPanel.SetActive(false);

        // After the coroutine ends, and event is triggered
        VMCKConnector.Instance.GetStructuresCallDone.AddListener(CreateStructurePreviews);
        VMCKConnector.Instance.StructureApprovalProccessChanged.AddListener(ApprovalProcessUpdated);
        StartCoroutine(VMCKConnector.Instance.GetStructures());
    }

    public void Update()
    {
        // While comments content is empty, disable submit new comment button
        submitNewComment.interactable = newCommentContent.text == "" ? false : true;
    }

    public void UpdateUserInfoPanel(string _name, string _username, string _email, string _role)
    {
        userInfoName.text = "Name:\n" + _name;
        userInfoUsername.text = "Username:\n" + _username;
        userInfoEmail.text = "Email:\n" + _email;
        userInfoRole.text = "Role:\n" + _role;
    }

    // This method is called once VMCK Connector is done with async call. It triggers and event.
    public void CreateStructurePreviews(string _response)
    {
        StructureJSONDeserialized[] deserializedStructures = JsonHelper.getJsonArray<StructureJSONDeserialized>(_response);

        foreach(var item in deserializedStructures)
        {
            GameObject gm = Instantiate(structurePreviewPrefab, allStructuresContent.transform);
            gm.GetComponent<Structure>().Initialize(item);
        }
    }

    public void ShowStructureDetail(Structure _structure)
    {
        // Hide other UI elements
        allStructuresPanel.SetActive(false);
        structureDetailPanel.SetActive(true);
        structureDetailTDObjectsPanel.SetActive(true);
        structureDetailApprovalPanel.SetActive(true);

        // Set header
        windowHeader.text = "Structure detail";

        // Fill out info panel with info
        structureDetailName.text = _structure.data.name;
        structureDetailAuthor.text = "Created by " + _structure.author.name;
        structureDetailDateCreated.text = "Date created: " + _structure.data.createdDate;
        structureDetailLocation.text = "Found at location LON: " + _structure.data.location.lon + ", LAT: " + _structure.data.location.lat + ", ALT: " + _structure.data.location.alt;
        structureDetailDescription.text = "Description\n" + _structure.data.description;

        // Prepare 3D Objects panel with all variants
        foreach (var item in _structure.data.allVariants)
        {
            GameObject gm = Instantiate(tdobjectPreviewPrefab, structureDetailTDObjectsContent.transform);
            gm.GetComponent<TDObject>().Initialize(item);
        }

        // Fecth approval process data from VMCK API
        VMCKConnector.Instance.GetApprovalProcessDone.AddListener(UpdateApprovalPanel);
        StartCoroutine(VMCKConnector.Instance.GetApprovalProcess(_structure.data.id));

        structureDetailTDObjectsPanel.SetActive(false);
        structureDetailApprovalPanel.SetActive(false);
    }

    public void UpdateApprovalPanel(string _response)
    {
        if(_response != null)
        {
            // Parse response from API
            ApprovalProcessJSONDeserialized data = JsonUtility.FromJson<ApprovalProcessJSONDeserialized>(_response);
        
            foreach (var item in data.modelIterations)
            {
                GameObject gm = Instantiate(modelIterationPrefab, structureDetailApprovalsContent.transform);
                gm.GetComponent<ModelIteration>().Initialize(item);
            }
            VMCKConnector.Instance.GetApprovalProcessDone.RemoveListener(UpdateApprovalPanel);
        }
    }

    public void ApprovalProcessUpdated(string _structureId)
    {
        // Show response, that everything went smoothly
        ShowInfoPanel("Approval process has changed for structure: " + _structureId);

        // Reload all structures
        RefreshAllStructures();
    }

    // Deletes all spawned prefabs of structure and TDObjects and gets them again from API and rerenders them
    private void RefreshAllStructures()
    {
        // Delete comment for iteration detail
        foreach (Transform child in iterationDetailContent.transform)
        {
            Destroy(child.gameObject);
        }

        // Delete Approvals for current structure
        foreach (Transform child in structureDetailApprovalsContent.transform)
        {
            Destroy(child.gameObject);
        }

        // Delete TDObjects for current structure
        foreach (Transform child in structureDetailTDObjectsContent.transform)
        {
            Destroy(child.gameObject);
        }
        // Delete all spawned structure previews
        foreach(Transform child in allStructuresContent.transform)
        {
            Destroy(child.gameObject);
        }
        // Set UI to main page
        allStructuresPanel.SetActive(true);
        structureDetailPanel.SetActive(false);
        iterationDetailPanel.SetActive(false);
        newCommentPanel.SetActive(false);

        // Ask via API for structures again
        StartCoroutine(VMCKConnector.Instance.GetStructures());
    }

    public void GoBackToAllStructuresPanel()
    {
        windowHeader.text = "List of all structures";

        // Delete Approvals for current structure
        foreach (Transform child in structureDetailApprovalsContent.transform)
        {
            Destroy(child.gameObject);
        }

        // Delete all TDObject variants (spawned prefabs)
        foreach (Transform child in structureDetailTDObjectsContent.transform)
        {
            Destroy(child.gameObject);
        }

        structureDetailPanel.SetActive(false);
        allStructuresPanel.SetActive(true);
    }

    public void GoBackToStructureDetail()
    {
        windowHeader.text = "Structure detail";

        foreach (Transform child in iterationDetailContent.transform)
        {
            Destroy(child.gameObject);
        }

        iterationDetailPanel.SetActive(false);
        structureDetailPanel.SetActive(true);
    }

    public void ShowApprovalIterationDetail(ModelIteration _iteration)
    {
        structureDetailPanel.SetActive(false);
        iterationDetailPanel.SetActive(true);
        historianActions.SetActive(false);
        graphicianActions.SetActive(false);

        // Set header
        windowHeader.text = "Approval iteration detail";

        // Prepare approve and decline buttons
        if (_iteration.data.historianApprovingId == "" && _iteration.data.historianDecliningId == "")
        {
            historianActions.SetActive(true);
            approveAsHistorian.onClick.AddListener(() => StartCoroutine(VMCKConnector.Instance.ApproveAsHistorian(_iteration.data.id)));
            declineAsHistorian.onClick.AddListener(() => StartCoroutine(VMCKConnector.Instance.DeclineAsHistorian(_iteration.data.id)));
        }
        if (_iteration.data.graphicianApprovingId == "" && _iteration.data.graphicianDecliningId == "")
        {
            graphicianActions.SetActive(true);
            approveAsGraphician.onClick.AddListener(() => StartCoroutine(VMCKConnector.Instance.ApproveAsGraphician(_iteration.data.id)));
            declineAsGraphician.onClick.AddListener(() => StartCoroutine(VMCKConnector.Instance.DeclineAsGraphician(_iteration.data.id)));
        }

        // Prepare new comment panel via its button
        submitNewComment.onClick.RemoveAllListeners();
        submitNewComment.onClick.AddListener(() => StartCoroutine(VMCKConnector.Instance.AddComment(_iteration.data.id, newCommentContent.text)));

        // Fetch all comments, that are linked to this iteration
        VMCKConnector.Instance.GetCommentsDone.AddListener(UpdateComments);
        StartCoroutine(VMCKConnector.Instance.GetComments(_iteration.data.id));
    }

    private void UpdateComments(string _response)
    {
        CommentJSONDeserialized[] comments = JsonHelper.getJsonArray<CommentJSONDeserialized>(_response);
        foreach(var item in comments)
        {
            GameObject gm = Instantiate(commentPrefab, iterationDetailContent.transform);
            gm.GetComponent<Comment>().Initialize(item);
        }
        VMCKConnector.Instance.GetCommentsDone.RemoveListener(UpdateComments);
    }

    public void UpdateNewCommentText(string _text)
    {
        if(newCommentContent.IsActive())
        {
            newCommentContent.text = _text;
        }
    }

    public void ShowErrorDetail(string _error)
    {
        // Disable all other panels
        allStructuresPanel.SetActive(false);
        structureDetailPanel.SetActive(false);
        iterationDetailPanel.SetActive(false);
        newCommentPanel.SetActive(false);

        errorDetailText.text = _error;
        errorDetailPanel.SetActive(true);
    }

    public void ShowInfoPanel(string _message)
    {
        infoPanel.SetActive(true);
        infoPanel.GetComponentInChildren<Text>().text = _message;
    }

    public void ExitApplication()
    {
        Application.Quit();
    }
}
