﻿using Newtonsoft.Json;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

[System.Serializable]
public class VMCKConnectorEvent : UnityEvent<string>
{
}

// Singleton class that makes all calls with VMCK backend private API. All calls are made asynchronously.
public class VMCKConnector : MonoBehaviour
{
    public static VMCKConnector Instance { get { return _instance; } }
    public VMCKConnectorEvent GetStructuresCallDone;
    public VMCKConnectorEvent GetModelCallDone;
    public VMCKConnectorEvent GetUsersDone;
    public VMCKConnectorEvent StructureApprovalProccessChanged;
    public VMCKConnectorEvent GetApprovalProcessDone;
    public VMCKConnectorEvent GetCommentsDone;
    public string VMCK_API_SERVER = "http://109.123.202.213:3000";

    // User info
    private string bearerToken;
    private string fullname;
    private string username;
    private string email;
    private string role;
    private static VMCKConnector _instance;

    // This class is a Singleton
    private void Awake()
    {
        if(_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        // Go through arguments obtained from Laucnher where user logged in
        string[] args = System.Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++)
        {
            if(args[i] == "-vmcktoken")
            {
                bearerToken = args[i + 1];
            }
            else if(args[i] == "-name")
            {
                fullname = args[i + 1];
            }
            else if (args[i] == "-username")
            {
                username = args[i + 1];
            }
            else if (args[i] == "-email")
            {
                email = args[i + 1];
            }
            else if (args[i] == "-role")
            {
                role = args[i + 1];
            }
        }
        // In Unity editor, use this static admin token just to test things out
#if UNITY_EDITOR
        bearerToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIxMDYwZmY1NC00ZWI1LTQ1MGYtYjQwZi0wZmJkYThkYzJlMTYiLCJpYXQiOjE2MTY1MDU4OTN9.C8VxscaCoXvp0qDZUDX-9cRNn0HYAir8ALLJlnRS1y0";
        fullname = "Unity Editor Service Account";
        username = "vmck_vr_viewer";
        email = "service_account@xd.com";
        role = "admin";
#endif  
        // Update main menu user info
        MainMenuManager.Instance.UpdateUserInfoPanel(fullname, username, email, role);
    }

    // Call VMCK API to get a list of all structures in database
    public IEnumerator GetStructures()
    {
        string url = VMCK_API_SERVER + "/structures";
        using(UnityWebRequest www = UnityWebRequest.Get(url))
        {
            www.SetRequestHeader("Authorization", "Bearer " + bearerToken);
            yield return www.SendWebRequest();
            if(www.isNetworkError || www.isHttpError)
            {
                CallFailed(url, www.error);
            }
            else
            {
                GetStructuresCallDone.Invoke(www.downloadHandler.text);
            }
        }
    }

    // Call VMCK API to get a specific model by its ID
    public IEnumerator GetModel(string _modelId)
    {
        string url = VMCK_API_SERVER + "/models/" + _modelId;
        using(UnityWebRequest www = UnityWebRequest.Get(url))
        {
            www.SetRequestHeader("Authorization", "Bearer " + bearerToken);
            yield return www.SendWebRequest();
            if(www.isNetworkError || www.isHttpError)
            {
                CallFailed(url, www.error);
            }
            else
            {
                GetModelCallDone.Invoke(www.downloadHandler.text);
            }
        }
    }

    // Call VMCK API to get all users
    public IEnumerator GetUsers()
    {
        string url = VMCK_API_SERVER + "/users";
        using (UnityWebRequest www = UnityWebRequest.Get(url))
        {
            www.SetRequestHeader("Authorization", "Bearer " + bearerToken);
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                CallFailed(url, www.error);
            }
            else
            {
                GetUsersDone.Invoke(www.downloadHandler.text);
            }
        }
    }

    public IEnumerator SubmitTDObjectForApproval(string _structureId, string _tdobjectId, string _tdobjectName)
    {
        // Check to see if there already is Approval proccess for this structure, if not create it
        string url = VMCK_API_SERVER + "/approvals?findByStructure=" + _structureId;
        using(UnityWebRequest req1 = UnityWebRequest.Get(url))
        {
            req1.SetRequestHeader("Authorization", "Bearer " + bearerToken);
            yield return req1.SendWebRequest();
            if (req1.isNetworkError || req1.isHttpError)
            {
                CallFailed(url, req1.error);
            }
            else
            {
                // Parse approval proccess response
                ApprovalProcessJSONDeserialized[] res1 = JsonHelper.getJsonArray<ApprovalProcessJSONDeserialized>(req1.downloadHandler.text);
                ApprovalProcessJSONDeserialized approvalProcess = null;

                // Create new approval proccess for parent structure, there is none. Via POST
                if(res1.Length == 0)
                {
                    url = VMCK_API_SERVER + "/approvals/" + _structureId;
                    WWWForm form1 = new WWWForm();
                    form1.AddField("name", "Approval process for " + _structureId);
                    using(UnityWebRequest req2 = UnityWebRequest.Post(url, form1))
                    {
                        req2.SetRequestHeader("Authorization", "Bearer " + bearerToken);
                        yield return req2.SendWebRequest();
                        if(req2.isNetworkError || req2.isHttpError)
                        {
                            CallFailed(url, req2.error);
                        }
                        else
                        {
                            // Parse response and save it
                            approvalProcess = JsonUtility.FromJson<ApprovalProcessJSONDeserialized>(req2.downloadHandler.text);
                        }
                    }
                }
                // There is alredy a approval process in place, save id
                else
                {
                    approvalProcess = res1[0];
                }
                // Approval process ready, create new model iteration for this 3D object as a part of this approval process
                url = VMCK_API_SERVER + "/approvals/" + approvalProcess.id + "/iterations/model";
                WWWForm form2 = new WWWForm();
                form2.AddField("name", _tdobjectName);
                form2.AddField("tdObjectId", _tdobjectId);
                using(UnityWebRequest req3 = UnityWebRequest.Post(url, form2))
                {
                    req3.SetRequestHeader("Authorization", "Bearer " + bearerToken);
                    yield return req3.SendWebRequest();
                    if(req3.isNetworkError || req3.isHttpError)
                    {
                        CallFailed(url, req3.error);
                    }
                    else
                    {
                        StructureApprovalProccessChanged.Invoke(_structureId);
                    }
                }
            }
        }
    }

    public IEnumerator GetApprovalProcess(string _structureId)
    {
        string url = VMCK_API_SERVER + "/approvals?findByStructure=" + _structureId;
        using(UnityWebRequest req1 = UnityWebRequest.Get(url))
        {
            req1.SetRequestHeader("Authorization", "Bearer " + bearerToken);
            yield return req1.SendWebRequest();
            if(req1.isNetworkError || req1.isHttpError)
            {
                CallFailed(url, req1.error);
            }
            else
            {
                // Parse approval process ID
                ApprovalProcessJSONDeserialized[] res1 = JsonHelper.getJsonArray<ApprovalProcessJSONDeserialized>(req1.downloadHandler.text);
                
                // None approval process
                if(res1.Length == 0)
                {
                    GetApprovalProcessDone.Invoke(null);
                }
                else
                {
                    // Send another GET to get more info about process, we have its ID now
                    url = VMCK_API_SERVER + "/approvals/" + res1[0].id;
                    using(UnityWebRequest req2 = UnityWebRequest.Get(url))
                    {
                        req2.SetRequestHeader("Authorization", "Bearer " + bearerToken);
                        yield return req2.SendWebRequest();
                        if(req2.isNetworkError || req2.isHttpError)
                        {
                            CallFailed(url, req2.error);
                        }
                        else
                        {
                            GetApprovalProcessDone.Invoke(req2.downloadHandler.text);
                        }
                    }
                }
            }
        }
    }

    public IEnumerator GetComments(string _iterationId)
    {
        string url = VMCK_API_SERVER + "/comments/model/" + _iterationId;
        using (UnityWebRequest www = UnityWebRequest.Get(url))
        {
            www.SetRequestHeader("Authorization", "Bearer " + bearerToken);
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                CallFailed(url, www.error);
            }
            else
            {
                GetCommentsDone.Invoke(www.downloadHandler.text);
            }
        }
    }

    public IEnumerator ApproveAsHistorian(string _iterationId)
    {
        string url = VMCK_API_SERVER + "/approvals/iterations/model/" + _iterationId + "/approval?as=historian";
        WWWForm form = new WWWForm();
        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            www.SetRequestHeader("Authorization", "Bearer " + bearerToken);
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                CallFailed(url, www.error);
            }
            else
            {
                StructureApprovalProccessChanged.Invoke(_iterationId);
            }
        }
    }

    public IEnumerator DeclineAsHistorian(string _iterationId)
    {
        string url = VMCK_API_SERVER + "/approvals/iterations/model/" + _iterationId + "/rejection?as=historian";
        WWWForm form = new WWWForm();
        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            www.SetRequestHeader("Authorization", "Bearer " + bearerToken);
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                CallFailed(url, www.error);
            }
            else
            {
                StructureApprovalProccessChanged.Invoke(_iterationId);
            }
        }
    }

    public IEnumerator ApproveAsGraphician(string _iterationId)
    {
        string url = VMCK_API_SERVER + "/approvals/iterations/model/" + _iterationId + "/approval?as=graphician";
        WWWForm form = new WWWForm();
        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            www.SetRequestHeader("Authorization", "Bearer " + bearerToken);
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                CallFailed(url, www.error);
            }
            else
            {
                StructureApprovalProccessChanged.Invoke(_iterationId);
            }
        }
    }

    public IEnumerator DeclineAsGraphician(string _iterationId)
    {
        string url = VMCK_API_SERVER + "/approvals/iterations/model/" + _iterationId + "/rejection?as=graphician";
        WWWForm form = new WWWForm();
        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            www.SetRequestHeader("Authorization", "Bearer " + bearerToken);
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                CallFailed(url, www.error);
            }
            else
            {
                StructureApprovalProccessChanged.Invoke(_iterationId);
            }
        }
    }

    public IEnumerator AddComment(string _iterationId, string _comment)
    {
        // Clear text fields for next comments
        MainMenuManager.Instance.newCommentContent.text = "";

        string url = VMCK_API_SERVER + "/comments/model/" + _iterationId;

        // Prepare body for POST
        var comment = new
        {
            type = "text",
            content = new
            {
                text = _comment
            }
        };
        string json = JsonConvert.SerializeObject(comment);
        var www = new UnityWebRequest(url, "POST");
        byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(json);
        www.uploadHandler = new UploadHandlerRaw(bodyRaw);
        www.downloadHandler = new DownloadHandlerBuffer();
        www.SetRequestHeader("Authorization", "Bearer " + bearerToken);
        www.SetRequestHeader("Content-Type", "application/json");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            CallFailed(url, www.error);
        }
        else
        {
            StructureApprovalProccessChanged.Invoke(_iterationId);
        }
    }

    private void CallFailed(string _url, string _error)
    {
        Debug.Log("Call to VMCK API failed.\nApplication was trying to access -> " + _url + ".\nAPI returned: " + _error);
        MainMenuManager.Instance.ShowErrorDetail("Call to VMCK API failed.\nApplication was trying to access -> " + _url + ".\nAPI returned: " + _error + "\n\nPlease try again later by restarting this application. \n\nPotential causes:\n- Your account is NEW or does not have enough priviliges. Contact project's role administrator.\n\n- API Server may be down. Check if server on IP address: " + VMCK_API_SERVER + " is running.\n\n- Make sure, you are opening the App via its Launcher and signing in. Do NOT run Unity standalone build!");
    }
}
