﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Structure : MonoBehaviour
{
    public Text previewNameTextField;
    public Text previewAuthorTextField;
    public StructureJSONDeserialized data;
    public UserJSONDeserialized author;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(ShowDetail);
    }

    // Structure data as input from VMCK API. Some of the child objects are only IDs, look them up via API
    public void Initialize(StructureJSONDeserialized _data)
    {
        data = _data;
        previewNameTextField.text = data.name;
        VMCKConnector.Instance.GetUsersDone.AddListener(SetAuthor);
        StartCoroutine(VMCKConnector.Instance.GetUsers());
    }

    // Triggered by event raised upon completion of async call to get all users from API
    public void SetAuthor(string _response)
    {
        UserJSONDeserialized[] deserializedUsers = JsonHelper.getJsonArray<UserJSONDeserialized>(_response);
        foreach(var user in deserializedUsers)
        {
            if(user.id == data.userId)
            {
                author = user;
                previewAuthorTextField.text = author.name;
            }
        }
        VMCKConnector.Instance.GetUsersDone.RemoveListener(SetAuthor);
    }

    private void ShowDetail()
    {
        MainMenuManager.Instance.ShowStructureDetail(this);
    }
}
