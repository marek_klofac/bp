﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StructureJSONDeserialized
{
    public string id;
    public string city;
    public string name;
    public string description;
    public string createdDate;
    public string href;
    public LocationJSONDeserialized location;
    public string userId;
    public TDObjectJSONDeserialized[] allVariants;
}
