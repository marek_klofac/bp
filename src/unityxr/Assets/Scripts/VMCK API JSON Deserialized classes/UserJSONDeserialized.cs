﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UserJSONDeserialized
{
    public string id;
    public string email;
    public string username;
    public string createdDate;
    public string name;
    public string role;
    public string status;
    public string deletedDate;
}
