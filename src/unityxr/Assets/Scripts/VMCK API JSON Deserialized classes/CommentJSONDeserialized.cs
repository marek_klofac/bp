﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CommentJSONDeserialized
{
    public string id;
    public string type;
    public ContentJSONDeserialized content;
    public string byId;
    public string createdDate;
    public UserJSONDeserialized by;
}
