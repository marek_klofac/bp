﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ApprovalProcessJSONDeserialized
{
    public string id;
    public string name;
    public bool active;
    public string phase;
    public string createdDate;
    public string structureId;
    public ModelIterationJSONDeserialized[] modelIterations;
}
