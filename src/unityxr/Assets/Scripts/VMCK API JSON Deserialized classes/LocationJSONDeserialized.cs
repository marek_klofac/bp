﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LocationJSONDeserialized
{
    public int lon;
    public int lat;
    public int alt;
}
