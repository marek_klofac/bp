﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TDObjectJSONDeserialized
{
    public string id;
    public string name;
    public bool isApprovedByApprover;
    public bool isApprovedByHistorian;
    public string createdDate;
    public string href;
    public int[] version;
    public string status;
    public string structureId;
    public string modelId;
    public string userId;
    public TextureJSONDeserialized[] textures;
}
