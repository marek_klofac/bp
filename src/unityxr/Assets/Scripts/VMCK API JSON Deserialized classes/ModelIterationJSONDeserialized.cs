﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ModelIterationJSONDeserialized
{
    public string id;
    public string name;
    public string approvalProcessId;
    public string tdObjectId;
    public string historianApprovingId;
    public string historianDecliningId;
    public string graphicianApprovingId;
    public string graphicianDecliningId;
    public string finalizedDate;
    public string createdDate;
    public CommentJSONDeserialized[] comments;
}
