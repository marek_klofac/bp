﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TextureJSONDeserialized
{
    public string id;
    public string filename;
    public string fileSize;
    public string uploadedDate;
    public string userId;
}
