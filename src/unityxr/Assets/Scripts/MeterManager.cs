﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MeterManager : MonoBehaviour
{
    public Transform start;
    public Transform end;
    public Canvas canvas;
    public Vector3 canvasOffset;

    private LineRenderer lineRenderer;

    // Start is called before the first frame update
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        // Set positions for line renderer
        lineRenderer.SetPosition(0, start.position);
        lineRenderer.SetPosition(1, end.position);

        // Set position of UI Canvas to be exactly halfway between two pivots
        canvas.transform.position = ((start.position + end.position) / 2) + canvasOffset;

        // Set rotation correctly, facing main camera
        Quaternion rotation = Camera.main.transform.rotation;
        Vector3 eulerRotation = rotation.eulerAngles;
        canvas.transform.rotation = Quaternion.Euler(0, eulerRotation.y, 0);

        // Update UI Canvas text
        Text text = canvas.GetComponentInChildren<Text>();
        float distance = Vector3.Distance(start.position, end.position) * 100;
        text.text = distance.ToString("F1") + "cm";
    }
}
