﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class ToolSelectorManager : MonoBehaviour
{
    public Vector3 menuHandOffset;
    public Transform userHeadTransform;
    public Vector3 toolSpawnPointOffset;
    private GameObject activeTool;

    public void Start()
    {
        gameObject.SetActive(false);
    }

    public void ChangeActiveStatus(Transform _handTransform)
    {
        // Menu is currently active, hide it and reset transform
        if(gameObject.activeSelf)
        {
            gameObject.SetActive(false);
        }
        // Menu is currently hidden, show it and snap to hand
        else
        {
            gameObject.SetActive(true);

            // Face the main camera
            transform.rotation = Camera.main.transform.rotation;

            // Adjust the rotation in Z axis, so that it is always zero (menu is always straight not dependent on user leaning head to the side)
            Vector3 eulerRotation = transform.rotation.eulerAngles;
            transform.rotation = Quaternion.Euler(eulerRotation.x, eulerRotation.y, 0);

            // Calculates offset in local space of hand and returns world coordinates
            transform.position = _handTransform.TransformPoint(menuHandOffset);
        }
    }

    public void SpawnTool(GameObject _tool)
    {
        ClearHands();

        // Spawn given tool prefab at hand position
        if(_tool != null)
        {
            // Rotate tool to face player
            Quaternion headRotation = Camera.main.transform.rotation;
            Vector3 eulerHeadRotation = headRotation.eulerAngles;
            Quaternion toolRotation = Quaternion.Euler(0, eulerHeadRotation.y - 180, 0);

            // Calculate position of tool using local space and offset from hand
            Vector3 toolPosition = userHeadTransform.TransformPoint(toolSpawnPointOffset);

            activeTool = Instantiate(_tool, toolPosition, toolRotation);
        }
    }

    public void ClearHands()
    {
        // Delete previous tool in hand
        if (activeTool != null)
        {
            // Bug fix - clear colliders before interactable object deletion. Otherwise interaction manager will start throwing exceptions
            // Check current root object for attached script of XR Grab Interactable, if there is, clear colliders
            if(activeTool.GetComponent<XRGrabInteractable>())
            {
                activeTool.GetComponent<XRGrabInteractable>().colliders.Clear();
            }
            // Look for XR Grab Interactable scripts in children. If there are some, clear also their colliders
            if(activeTool.GetComponentsInChildren<XRGrabInteractable>().Length > 0)
            {
                XRGrabInteractable[] interactables = activeTool.GetComponentsInChildren<XRGrabInteractable>();
                foreach(var interactable in interactables)
                {
                    interactable.colliders.Clear();
                }
            }
            Destroy(activeTool);
        }
        // Hide menu
        ChangeActiveStatus(null);
    }
}
