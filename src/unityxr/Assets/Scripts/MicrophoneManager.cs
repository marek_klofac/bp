﻿using GoogleCloudStreamingSpeechToText;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MicrophoneManager : MonoBehaviour
{
    public Renderer microphoneIndicator;
    public Material indicatorOnMaterial;
    public Material indicatorOffMaterial;

    public void Start()
    {
        GetComponent<StreamingRecognizer>().onInterimResult.AddListener(MainMenuManager.Instance.UpdateNewCommentText);    
    }

    // Function called, once user pressed trigger button while holding microphone to enable recognizer
    public void SetIndicatorToOn()
    {
        microphoneIndicator.material = indicatorOnMaterial;
    }

    // Function called, once user released trigger button while holding microphone to disable recognizer
    public void SetIndicatorToOff()
    {
        microphoneIndicator.material = indicatorOffMaterial;
    }
}
