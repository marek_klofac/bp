﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TDObject : MonoBehaviour
{
    public Text nameTextField;
    public Text authorTextField;
    public Text versionTextField;
    public Text statusTextField;
    public Button actionButton;
    public TDObjectJSONDeserialized data;

    // TDObject data as input from VMCK API. Some of the child objects are only IDs, look them up via API
    public void Initialize(TDObjectJSONDeserialized _data)
    {
        data = _data;
        nameTextField.text = data.name;
        versionTextField.text = data.version[0].ToString() + "." + data.version[1].ToString() + "." + data.version[2].ToString();
        statusTextField.text = data.status.ToUpper();

        // Aynschronosly look up 3D object author to later get his name
        VMCKConnector.Instance.GetUsersDone.AddListener(SetAuthor);
        StartCoroutine(VMCKConnector.Instance.GetUsers());

        // FINISHED status means, the object can be sent to approval process
        if(data.status == "finished")
        {
            actionButton.GetComponentInChildren<Text>().text = "Submit for approval";
            actionButton.onClick.AddListener(() => StartCoroutine(VMCKConnector.Instance.SubmitTDObjectForApproval(data.structureId, data.id, data.name)));
        }
        // Otherwise the default action is to inspect the 3D object in VR (download and import)
        else
        {
            actionButton.GetComponentInChildren<Text>().text = "Inspect in VR";
            actionButton.onClick.AddListener(() => ModelLoader.Instance.DownloadModel(data.modelId));
        }
    }

    // Triggered by event raised upon completion of async call to get all users from API
    public void SetAuthor(string _response)
    {
        UserJSONDeserialized[] deserializedUsers = JsonHelper.getJsonArray<UserJSONDeserialized>(_response);
        foreach(var user in deserializedUsers)
        {
            if(user.id == data.userId)
            {
                authorTextField.text = user.name;
            }
        }
        VMCKConnector.Instance.GetUsersDone.RemoveListener(SetAuthor);
    }
}
