﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Comment : MonoBehaviour
{
    public Text author;
    public Text dateCreated;
    public Text comment;
    public CommentJSONDeserialized data;

    public void Initialize(CommentJSONDeserialized _data)
    {
        data = _data;
        author.text = data.by.name + " (" + data.by.role + ")";
        dateCreated.text = data.createdDate;
        comment.text = data.content.text;
    }
}
