﻿using Auth0.OidcClient;
using IdentityModel.OidcClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Windows;
using System.Configuration;
using System.Net.Http;
using System.Windows.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace launcher
{
    enum LauncherStatus
    {
        ready,
        failed,
        downloadingGame,
        downloadingUpdate
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string rootPath;
        private string extractDirPath;
        private string versionFile;
        private string gameZip;
        private string gameExe;
        private Auth0Client client;

        // User info obtained from VMCK API
        private string vmckToken;
        private string name;
        private string username;
        private string email;
        private string role;

        private LauncherStatus status;
        internal LauncherStatus Status
        {
            get => status;
            // Upon the change of launcher status, update the UI text of a button
            set
            {
                status = value;
                switch(status)
                {
                    case LauncherStatus.ready:
                        PlayButton.Content = "Play";
                        break;
                    case LauncherStatus.failed:
                        PlayButton.Content = "Failed, click retry";
                        break;
                    case LauncherStatus.downloadingGame:
                        PlayButton.Content = "Downloading game...";
                        break;
                    case LauncherStatus.downloadingUpdate:
                        PlayButton.Content = "Downloading updates...";
                        break;
                    default:
                        break;
                }
            }
        }

        // Initialize UI and prepare OS paths for build file and executables
        public MainWindow()
        {
            InitializeComponent();
            rootPath = Directory.GetCurrentDirectory();
            extractDirPath = Path.Combine(rootPath, "build");
            versionFile = Path.Combine(rootPath, "version.txt");
            gameZip = Path.Combine(rootPath, "build.zip");
            gameExe = Path.Combine(rootPath, "build", "unityxr.exe");
        }

        // Using web sockets, check remote file repository for current version of the game
        private void CheckForUpdates()
        {
            // Game is already installed and there is a version file
            if(File.Exists(versionFile))
            {
                Version localVersion = new Version(File.ReadAllText(versionFile));
                VersionText.Text = localVersion.ToString();

                // Initiate web client connection to specified remote file repository to download the version file
                try
                {
                    WebClient webClient = new WebClient();
                    Version remoteVersion = new Version(webClient.DownloadString(ConfigurationManager.AppSettings["VERSION_REMOTE_LOCATION"]));

                    // There has been a new release of the game, download it and install it
                    if (remoteVersion.IsDifferentThan(localVersion))
                    {
                        InstallGameFiles(true, remoteVersion);
                    }
                    // Local version of the game is up to date
                    else
                    {
                        Status = LauncherStatus.ready;
                    }
                }
                // Trouble with communicating with the remote file repository, maybe net issues?
                catch(Exception _e)
                {
                    Status = LauncherStatus.failed;
                    MessageBox.Show($"Error checking for game updates: {_e}");
                }
            }
            // There is no version file, meaning this is the first time and game has to be installed
            else
            {
                InstallGameFiles(false, Version.zero);
            }
        }

        // Opens up the connection to remote file repository, where the latest game files are stored. In case of fresh install also downloads latest version file
        private void InstallGameFiles(bool _isUpdate, Version _remoteVersion)
        {
            try
            {
                WebClient webClient = new WebClient();
                if(_isUpdate)
                {
                    Status = LauncherStatus.downloadingUpdate;
                }
                else
                {
                    Status = LauncherStatus.downloadingGame;
                    _remoteVersion = new Version(webClient.DownloadString(ConfigurationManager.AppSettings["VERSION_REMOTE_LOCATION"]));
                }

                // Start downloading the game files asynchronously, otherwise it would stop code execution, register callback
                webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadGameCompletedCallback);
                webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadGameProgressCallback);
                webClient.DownloadFileAsync(new Uri(ConfigurationManager.AppSettings["BUILD_REMOTE_LOCATION"]), gameZip, _remoteVersion);
            }
            // Trouble with communicating with the remote file repository, maybe net issues?
            catch (Exception _e)
            {
                Status = LauncherStatus.failed;
                MessageBox.Show($"Error downloading game files: {_e}");
            }
        }

        // Callback function, triggered upon the completion of download of game files
        private void DownloadGameCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            try
            {
                // Parse remote version from response
                string remoteVersion = ((Version)e.UserState).ToString();

                // Extract download build file, delete old build and create new version file
                if (Directory.Exists(extractDirPath))
                {
                    Directory.Delete(extractDirPath, true);
                }
                ZipFile.ExtractToDirectory(gameZip, extractDirPath);
                
                File.Delete(gameZip);
                File.WriteAllText(versionFile, remoteVersion);

                // Update UI after game is installed
                VersionText.Text = remoteVersion;
                ProgressBar.Visibility = Visibility.Hidden;
                ProgressText.Visibility = Visibility.Hidden;
                Status = LauncherStatus.ready;
            }
            // Trouble with communicating with the remote file repository, maybe net issues?
            catch (Exception _e)
            {
                Status = LauncherStatus.failed;
                MessageBox.Show($"Error installing game files: {_e}");
            }
        }

        // Callback function, triggered each time the file gets downloaded some more
        private void DownloadGameProgressCallback(object sender, DownloadProgressChangedEventArgs e)
        {
            // Display progress bar
            ProgressBar.Visibility = Visibility.Visible;
            ProgressText.Visibility = Visibility.Visible;
            ProgressBar.Value = e.ProgressPercentage;
            ProgressText.Text = $"Downloaded {e.BytesReceived} of {e.TotalBytesToReceive}. {e.ProgressPercentage}% completed";
        }

        // Callback function, triggered upon the completion of window rendering on users screen, check for any updates
        private void Window_ContentRendered(object sender, EventArgs e)
        {
            // Hide appropriate UI elements until user is logged in
            PlayButton.Visibility = Visibility.Hidden;
            PlayText.Visibility = Visibility.Hidden;
            LogoutButton.Visibility = Visibility.Hidden;
            LogoutText.Visibility = Visibility.Hidden;
            ProgressBar.Visibility = Visibility.Hidden;
            ProgressText.Visibility = Visibility.Hidden;
        }

        // Callback function, triggered upon the click on a play button
        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            // Executable build of the game is present and launcher is ready
            if(File.Exists(gameExe) && Status == LauncherStatus.ready)
            {
                // Launch the game by starting a new process and pass it the received token from VMCK api and additional user info
                ProcessStartInfo startInfo = new ProcessStartInfo(gameExe, $"-vmcktoken {vmckToken} -name {name} -username {username} -email {email} -role {role}");
                startInfo.WorkingDirectory = Path.Combine(rootPath, "build");
                Process.Start(startInfo);

                // Close the launcher window
                Close();
            }
            // If anything failed, if user clicks the button again, it will try to check for updates again
            else if(Status == LauncherStatus.failed)
            {
                CheckForUpdates();
            }
        }

        // Callback function, triggered upon user click's on logout button
        private async void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            if(status == LauncherStatus.ready)
            {
                // Sign out of Google
                await client.LogoutAsync(true);

                // Udpate UI
                LoginButton.Visibility = Visibility.Visible;
                LoginText.Visibility = Visibility.Visible;

                PlayButton.Visibility = Visibility.Hidden;
                PlayText.Visibility = Visibility.Hidden;
                LogoutButton.Visibility = Visibility.Hidden;
                LogoutText.Visibility = Visibility.Hidden;
                ProgressBar.Visibility = Visibility.Hidden;

                LogoutText.Text = "";
            }
            else
            {
                MessageBox.Show("Launcher is currently downloading and installing the game. Please wait.");
            }
        }

        // Callback function, triggered upon the click on a login button. Implementation of Auth0 web service, more here: https://auth0.com/docs/quickstart/native/wpf-winforms
        private async void LoginButton_Click(object sender, RoutedEventArgs e)
        {

            // This client will connect for use to Google's OAuth app
            client = new Auth0Client(new Auth0ClientOptions
            {
                Domain = ConfigurationManager.AppSettings["AUTH0_DOMAIN"],
                ClientId = ConfigurationManager.AppSettings["AUTH0_CLIENT_ID"]
            });

            // Specify, that login should be only via Google OAuth2
            var extraParameters = new Dictionary<string, string>();
            extraParameters.Add("connection", "google-oauth2");

            // Asynchronously await for user to finish logging in
            LoginResult loginResult = await client.LoginAsync(extraParameters: extraParameters);

            if (loginResult.IsError)
            {
                Status = LauncherStatus.failed;
                MessageBox.Show($"An error occurred during login. Check internet connection and try again please. Error message: {loginResult.Error}.");
            }
            // Obtained valid Google response with our id_token
            else
            {
                // Send obtained Google id token to VMCK api for authentication
                LoginToVMCKAPI(loginResult.IdentityToken);
            }
        }

        // This function communicates via REST API with VMCK backend and sends it 
        private async void LoginToVMCKAPI(string _googleIdToken)
        {
            try
            {
                HttpClient client = new HttpClient();
                var values = new Dictionary<string, string>
                {
                    { "token", _googleIdToken },
                    { "clientId", ConfigurationManager.AppSettings["GCP_CLIENT_ID"] }
                };
                var content = new FormUrlEncodedContent(values);
                var response = await client.PostAsync(ConfigurationManager.AppSettings["VMCK_API_SERVER"] + "/auth/googlesignin", content);
                var responseString = await response.Content.ReadAsStringAsync();

                if (response.IsSuccessStatusCode)
                {
                    // Save VMCK token from response and update UI
                    JObject json = JObject.Parse(responseString);
                    vmckToken = json["token"].ToString();
                    name = json["user"]["name"].ToString();
                    username = json["user"]["username"].ToString();
                    email = json["user"]["email"].ToString();
                    role = json["user"]["role"].ToString();
                    LogoutText.Text = $"Name: {json["user"]["name"].ToString()}\nUsername: {json["user"]["username"].ToString()}\nEmail: {json["user"]["email"].ToString()}\nRole: {json["user"]["role"].ToString()}";

                    // Update UI
                    LoginSuccessful();
                }
                // Other status codes, failed
                else
                {
                    Status = LauncherStatus.failed;
                    MessageBox.Show($"VMCK API returned non OK status code. Response message: {responseString}");
                }
            }
            // Trouble with communicating with VMCK api server
            catch (Exception _e)
            {
                Status = LauncherStatus.failed;
                MessageBox.Show($"Error communicating with VMCK API. Check internet connection. Error message: {_e}");
            }
        }

        // Updates UI scheme after a successful login process to VMCK api, passing in vmck api response to be parsed
        private async void LoginSuccessful()
        {
            // Hide LoginButton and LoginText
            LoginButton.Visibility = Visibility.Hidden;
            LoginText.Visibility = Visibility.Hidden;

            // Show PlayButton and PlayText
            PlayButton.Visibility = Visibility.Visible;
            PlayText.Visibility = Visibility.Visible;
            LogoutButton.Visibility = Visibility.Visible;
            LogoutText.Visibility = Visibility.Visible;

            // Wait for UI to update
            await Task.Delay(2000);

            // Start looking for up to date version of the game (download and install it)
            CheckForUpdates();
        }
    }

    // Internal helper struct for versions
    struct Version
    {
        internal static Version zero = new Version(0, 0, 0);
        private short major;
        private short minor;
        private short subMinor;

        internal Version(short _major, short _minor, short _subMinor)
        {
            major = _major;
            minor = _minor;
            subMinor = _subMinor;
        }

        internal Version(string _version)
        {
            string[] versionStrings = _version.Split('.');
            if(versionStrings.Length != 3)
            {
                major = 0;
                minor = 0;
                subMinor = 0;
                return;
            }
            major = short.Parse(versionStrings[0]);
            minor = short.Parse(versionStrings[1]);
            subMinor = short.Parse(versionStrings[2]);
        }
        // Function, that checks whether given version is the same as currently running or not, returns a boolean
        internal bool IsDifferentThan(Version _otherVersion)
        {
            if(major != _otherVersion.major)
            {
                return true;
            }
            else
            {
                if(minor != _otherVersion.minor)
                {
                    return true;
                }
                else
                {
                    if(subMinor != _otherVersion.subMinor)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        // Specify how the version should be converted to string
        public override string ToString()
        {
            return $"{major}.{minor}.{subMinor}";
        }
    }
}
